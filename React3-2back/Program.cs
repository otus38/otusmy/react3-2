const string Origin = "MyAllowSpecificOrigins";

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: Origin, x =>
        {
            x.WithOrigins(builder.Configuration.GetSection("CORS:Origins").Get<string[]>())
                .WithHeaders(builder.Configuration.GetSection("CORS:Headers").Get<string[]>())
                .WithMethods(builder.Configuration.GetSection("CORS:Methods").Get<string[]>());
        });
});

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseCors(Origin);

app.MapControllers();

app.Run();
